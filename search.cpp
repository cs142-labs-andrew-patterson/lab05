//Actual sorting functions
//Andrew Patterson
//cs142 2018-02-19

#include <cstring>
#include <iostream>
#include <sstream>

#include "search.h"

int binarySearch(long int term, int *list, long int length)
{	//Returns -1 if doesn't find the search term
	long int address = -1;
	long double searchArea = (double(length) - 1)/2.0;
	long double location = searchArea;
	bool searching = true;
	int cmpResult;

	while (searching)
	{
		if (term == list[round2Int(location)])
		{														//If have, 
			address = round2Int(location);						//Say the address, stop searching
			searching = false;
		}
		else
		{														//Otherwise
			searchArea /= 2;
			if (round2Int(searchArea) == 0)
			{													//If the space between is now zero, 
				searching = false;								//Stop searching
			}
			else if (term < list[round2Int(location)])
			{													//If the term is earlier,
				location -= searchArea;							//Check half the distance earlier
			}
			else
			{													//If later, 
				location += searchArea;							//Check later
			}
		}
	}

	return address;
}

int linearSearch(long int term, int *list, long int length)
{
	int address = -1;

	for (int i = 0; i < length; i++)
	{
		if (term == list[i])
		{
			address = i;
			break;
		}
	}

	return address;
}

long int round2Int(long double number)
{
	if ((number - (long int)(number)) > 0.5)
	{
		return (long int)(number + 1);
	}
	else
	{
		return (long int)(number);
	}
}

void printTableLine(long int numElements, double linearTime, double binaryTime, bool isCSV)
{
	//Make the numbers std::strings
	std::ostringstream numConvertTo;

	//Number of Elements searched
	numConvertTo << numElements;
	int spacesAfterElements = SPACE_PER_BOX - numConvertTo.str().size();
	//Clear stringstream (because _obviously_ there can't just be an actual "clear" func...)
	numConvertTo.str(std::string());

	//Linear search time
	numConvertTo << linearTime;
	int spacesAfterLinear = SPACE_PER_BOX - numConvertTo.str().size();
	numConvertTo.str(std::string());

	//Print number of elements and following spaces
	std::cout << numElements;
	printOutputDivs(spacesAfterElements, isCSV);

	//Time for linear search and spaces
	std::cout << linearTime;
	printOutputDivs(spacesAfterLinear, isCSV);

	//Time for binary search
	std::cout << binaryTime << std::endl;
}

void printOutputDivs(int spaces, bool isCSV, std::ostream &out)
{
	//This way it is easy to change between csv and pretty print
	if (isCSV)
		out << ',';
	else
	{
		for (int i = 0; i < spaces; i++)
		{
			out << ' ';
		}
	}
}