//Program to test behavior of linear search and binary search
//Andrew Patterson
//cs142
//2018-03-01 -- 2018-03-12

#include <iostream>
#include <climits>
#include <ctime>
#include <cassert>
#include "timer.h"
#include "search.h"

//The results, graphs, and commentary are all in .\data\results.xlsx

//I wrote a macro for timing things when I did this earlier (searched strings,
//so can't reuse everything directly)  Here it is reworked for this
#define TIME_FUNCTION(return_double, function, ...) \
	timer.reset();\
	function ( __VA_ARGS__ );\
	return_double = timer.getElapsedSeconds();\

using namespace std;

int main (int argc, char** argv)
{
	bool CSVp = false;	//Yes, this convention is taken from the language you think it does.  Probably
	if (argc > 1)
	{
		CSVp = true;
	}
	//Everything that includes CSVp can switch between csv output and pretty tabulated output,
	//for ease of machine parsing

	//Showing that yes, these search things do work
	const int TEST_ARRAY_SIZE = 25;
	int testArray[TEST_ARRAY_SIZE];
	for (int i = 0; i < TEST_ARRAY_SIZE; ++i) testArray[i] = i;	

	assert(testArray[linearSearch(22, testArray, TEST_ARRAY_SIZE)] == 22);
	assert(testArray[binarySearch(12, testArray, TEST_ARRAY_SIZE)] == 12);

	//set up the timer
	Timer timer; timer.init ();

	//Starting array, so only delete [] once
	int *array = new int[1 << 29];

	//Titles
	cout << "Num elements";
	printOutputDivs(SPACE_PER_BOX-12, CSVp);
	cout << "Time: Linear";
	printOutputDivs(SPACE_PER_BOX-12, CSVp);
	cout << "Time: Binary" << endl;

	for (int sizeLog2 = 0; sizeLog2 < 30; sizeLog2++)		//Any larger and it starts eating all my swap
	{
		//Make array
		int arraySize = 1 << sizeLog2;						//Loop through, making arraySize == 2**sizeLog2 (hence why it's sizeLog2 instead of size)
		for (int i = 0; i < arraySize; ++i) array[i] = i;	//Fill array, nearly the only original code left here

		double linearTime = 0;
		//time linear search
		TIME_FUNCTION(linearTime, linearSearch, arraySize, array, arraySize);

		//time binary search
		double binaryTime = 0;
		TIME_FUNCTION(binaryTime, binarySearch, arraySize, array, arraySize);

		printTableLine(arraySize, linearTime, binaryTime, CSVp);
	}

	//Ending stuff
	delete[] array;
	system ("pause");
	return 0;
}
