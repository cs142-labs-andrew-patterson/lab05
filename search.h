//Algorithms assigned in the O notation section
//Andrew Patterson
//cs142 2018-02-19

#ifndef SEARCH_H
#define SEARCH_H

#include <cstring>
#include <iostream>

const int SPACE_PER_BOX = 20;

int binarySearch(long int term, int *list, long int length);

int linearSearch(long int term, int *list, long int length);

long int round2Int(long double number);

void printTableLine(long int numElements, double linearTime, double binaryTime, bool isCSV);

void printOutputDivs(int spaces, bool isCSV, std::ostream &out = std::cout);

#endif
