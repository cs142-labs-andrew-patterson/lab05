#include <iostream>

#include "timer.h"

#if defined(_WIN32)
#include <windows.h>

void Timer::init()
{
  if (!QueryPerformanceFrequency (&ticksPerSecond_))
	  throw CantCreateTimer (); // system doesn't support hi-res timer; give up!

  QueryPerformanceCounter (&startTime_); 
  initialized_ = true;
} 

double Timer::getElapsedSeconds()
{
	if (! initialized_) throw NotInitialized();

	static LARGE_INTEGER lastTime = startTime_;
	LARGE_INTEGER currentTime;

	QueryPerformanceCounter(&currentTime);

	double seconds =  ((double) currentTime.QuadPart - (double)lastTime.QuadPart) 
									/ (double) ticksPerSecond_.QuadPart;

	// reset the timer
	lastTime = currentTime;

	return seconds;
} 
#elif defined (__unix__)
#include <unistd.h>
#include <ctime>

void Timer::init()
{
	clock_gettime(CLOCK_MONOTONIC, &startTime_);
	initialized_ = true;
}

double Timer::getElapsedSeconds()
{
	if (!initialized_) throw NotInitialized();
	timespec currentTime;
	static timespec lastTime = startTime_;

	clock_gettime(CLOCK_MONOTONIC, &currentTime);

	double seconds = (currentTime.tv_sec - lastTime.tv_sec) +\
					 ((double)(currentTime.tv_nsec - lastTime.tv_nsec) /\
					  (double)NANO_CONV);

	//reset with the power of static!
	lastTime = currentTime;

	return seconds;
}

#endif
