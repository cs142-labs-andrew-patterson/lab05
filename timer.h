#include <iostream>

#ifndef TIMER_H
#define TIMER_H

#if defined(_WIN32)
	#include <windows.h> //for high res timer stuff
	#define MEMBER_TYPE LARGE_INTEGER
#elif defined(__unix__)
	#include <unistd.h>
	#include <ctime>
	#define MEMBER_TYPE timespec
#endif
class Timer  
{
public:
	const int NANO_CONV = 1000000000;

	Timer() : initialized_ (false) { } 
	class CantCreateTimer {};
	class NotInitialized  {};

	void init();

	double getElapsedSeconds ();

	void reset () { getElapsedSeconds (); }

private:
	MEMBER_TYPE startTime_;
	MEMBER_TYPE ticksPerSecond_;
	bool          initialized_;
};
#endif 
