# Basic Makefile, as in my CS 14{1,2} txtbook
# (Thanks, Briggs!)

# Change to match actual program doing
PROG	:= searching-test
SRCS	:= $(wildcard *.cpp)
OBJS	:= ${SRCS:.cpp=.o}

$(PROG):	$(OBJS)
			$(CXX) -std=c++11 -g -o $@ $^

%.o:		%.cpp
			$(CXX) -std=c++11 -g -c $<

clean:
			rm -f $(PROG)
			rm -f $(OBJS)
			rm -rf Debug
			rm -rf x64

make.depend:	$(SRCS)
			$(CXX) -MM $^ > $@

-include make.depend
